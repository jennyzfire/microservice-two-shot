import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

from shoes_rest.models import BinVO
# from shoes_rest.models import Something

def get_bins():
    url = "http://wardrobe-api:8000/api/bins/"
    print('Shoes poller polling for data')
    response = requests.get(url)
    content = json.loads(response.content)
    for bins in content["bins"]:
        BinVO.objects.update_or_create(
            import_href = bins["href"],
            defaults={
                "closet_name": bins["closet_name"],
                "bin_number": bins["bin_number"],
                "bin_size": bins["bin_size"],
            }
        )

def poll():
    while True:
        print("polling for data")
        try:
            get_bins()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(5)


if __name__ == "__main__":
    poll()
