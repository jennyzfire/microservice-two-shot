import React from 'react';

class HatsForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            locations: [],
            name: '',
            fabric: '',
            styleName: '',
            color: '',
            location: '',
        };
        this.handleNameChange = this.handleNameChange.bind(this); //these are all technically event listeners 
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }; //copies all of the properties and values from this.hat into a new object (dictionary)
        data.style_name = data.styleName
        delete data.styleName
        delete data.locations


        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
    
            // const newHat = await response.json();
            // console.log(newHat);

            const cleared = {
                name: '',
                fabric: '',
                styleName: '',
                color: '',
                location: '',
            };
            this.setState(cleared);
        }
    }



    // All The Basic Handles 

    handleNameChange(event) { // handleNameChange = (e) anonymous function not attatched to a name 
        const value = event.target.value;
        this.setState({ name: value })
    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value })
    }
    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({ styleName: value })
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }


    // Complicated asyncs and Mount stuff 

    async componentDidMount() { //comp did mount is something that happens right when the page starts // async means you need time to process somthing
        const locationUrl = 'http://localhost:8100/api/locations/';
        const response = await fetch(locationUrl); // fetch is a GET by default
        if (response.ok) {
            const data = await response.json()
            console.log(data)
            this.setState({
                locations: data.locations
            })
        }
    }


    
    // async componentDidMount() {
    //     const url = 'http://localhost:3000/api/hats/';

    //     const response = await fetch(url); // fetch is a GET by default

    //     if (response.ok) {
    //         const data = await response.json();
    //         //this.setState({hats: data.hat}); //Go over what this actually does
    //     }
    // }



    // Converted HTML

    render() {
        return (
            <div className='my-5' container='true'>
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new Hat</h1>


                            {/* Hat Name Form */}
                            
                                <div className='col'>
                                    <div className='card shadow'>
                                    <div className='card-body'>
                                        <form className='form' onSubmit={this.handleSubmit} id="create-hat-form">
                                            <div className='row'>
                                                <div className='col'>
                                                    <div className="form-floating mb-3">
                                                        <input onChange={this.handleNameChange} placeholder="Name" required
                                                            type="text" name="name" id="name"
                                                            className="form-control" value={this.state.name} />
                                                        <label htmlFor="name">Name</label>
                                                    </div>
                                                </div>


                                                {/* Hat Fabric Name */}
                                                <div className='col'>
                                                    <div className="form-floating mb-3">
                                                        <input onChange={this.handleFabricChange} placeholder="fabric" required
                                                            type="text" name="fabric" id="fabric"
                                                            className="form-control" value={this.state.fabric} />
                                                        <label htmlFor="Fabric">Fabric</label>
                                                    </div>
                                                </div>

                                                {/* Style Name */}
                                                <div className='col'>
                                                    <div className="form-floating mb-3">
                                                        <input onChange={this.handleStyleChange} placeholder="style" required
                                                            type="text" name="style_name" id="style_name"
                                                            className="form-control" value={this.state.styleName} />
                                                        <label htmlFor="styleName">Style</label>
                                                    </div>
                                                </div>

                                                {/* Color */}
                                                <div className='col'>
                                                    <div className="form-floating mb-3">
                                                        <input onChange={this.handleColorChange} placeholder="Color" required
                                                            type="text" name="color" id="color"
                                                            className="form-control" value={this.state.color} />
                                                        <label htmlFor="color">Color</label>
                                                    </div>
                                                </div>


                                                {/* Locations complicated select dealyo */}
                                                {/* <div className='col'> */}
                                                    <div className="mb-3">
                                                        <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select" value={this.state.location}>
                                                            <option>Choose a location for your hat</option>
                                                            {this.state.locations.map(location => {
                                                                return (
                                                                    <option key={location.id} value={location.id}>
                                                                        {location.closet_name}
                                                                    </option>
                                                                );
                                                            })}
                                                        </select>
                                                    </div>
                                                </div>
                                            {/* </div> */}
                                            <button className="btn btn-primary">Make Hat</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        );
    };
}

export default HatsForm;
