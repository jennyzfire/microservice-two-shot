from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse

# Create your models here.

#Make a Location VO
class LocationVO(models.Model): 
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()


    # def get_api_url(self):
    #     return reverse("api_location", kwargs={"pk": self.pk})

    # def __str__(self):
    #     return self.closet_name


#Here is my awesome hat model
class Hat(models.Model):
    """
    This Hat model describes the fabric, its style name, its color, 
    a URL for a picture, and the location in the wardrobe where it exists.
    """
    name = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)

    # picture_url = models.URLField(null=True)

    #This becomes Location with a one to many relationship with the Location VO Above
    # Foreign key is a one to many
    location = models.ForeignKey( 
        LocationVO,
        related_name="locations",  # do not create a related name on LocationVO
        on_delete=models.PROTECT,
    )
    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("api_delete_hats", kwargs={"pk": self.pk})
