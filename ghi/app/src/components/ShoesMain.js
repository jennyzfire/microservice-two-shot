import React, { useState, useEffect} from "react";
import ShoeForm from "./ShoeForm";
import ReadOnlyRow from "./ReadOnlyRow";
import EditableRow from "./EditableRow"


const ShoesMain = () => {

    const[shoes, setShoes] = useState([]);

    async function getShoes() {
        let listUrl = "http://localhost:8080/shoes/"
        //get the list of shoes
        // let allData = [];
        try{
            const response = await fetch(listUrl);
            const listOfShoes = await response.json();
            setShoes(listOfShoes.shoes);
            } catch (e) {
            console.error(e);
        }
    }

    const handleDeleteClick = async (shoe) => {
        // event.preventDefault();
        const deleteUrl = `http://localhost:8080/shoes/${shoe}/`
        const fetchConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        let response = await fetch(deleteUrl, fetchConfig)
        // if(response.ok){
        //     const shoesCopy = [...shoes]
        //     const index = shoes.findIndex(shoe => {shoe.id === shoeToDelete.id});
        //     shoesCopy.splice(index, 1);
        //     setShoes(shoesCopy);
        // }
    }

    useEffect(
        () => {
         getShoes();
        }, [],
    )

    return(<div className="app-container">
            <form>
                <table>
                    <thead>
                        <tr>
                            <th>Brand</th>
                            <th>Model</th>
                            <th>Color</th>
                            <th>Image</th>
                            <th>Bin Number</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {shoes.map((shoe) => {
                        return(
                        <tr key={shoe.id}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.image}</td>
                            <td>{shoe.binVO.bin_number}</td>
                            <td><button onClick={() => handleDeleteClick(shoe.id)}>Delete</button></td>
                        </tr>
                        )
                        })}
                    </tbody>
                </table>
            </form>
            <ShoeForm />
            </div>);
};

export default ShoesMain;
