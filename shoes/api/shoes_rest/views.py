from .models import Shoe, BinVO
from common.encoder import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


class BinDetailEncoder(ModelEncoder):
    model = BinVO,
    properties = [
        "bin_number",
        "bin_size",
        "bin_number",
        "import_href",
    ]


class BinNumberEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "bin_number",
        "import_href",
    ]


# get a lit of all shoes
class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "image",
        "color",
        "binVO"
        ]

    encoders = {
        "binVO": BinNumberEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "image",
        "binVO"
        ]

    encoders = {
        "binVO": BinDetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_all_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(binVO=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    # if request.method == "GET":
    #     shoes = Shoe.objects.all()
    #     return JsonResponse(
    #         shoes,
    #         encoder=ShoeListEncoder,
    #         safe=False
    #     )
    # else: #POST
    #     content = json.loads(request.body)
    #     binVO = BinVO.objects.all()
    #     content["binVO"] = binVO
    #     shoe = Shoe.objects.create(**content)
    #     return JsonResponse(
    #         shoe, encoder=ShoeDetailEncoder,
    #         safe=False
    #     )
    else:
        content = json.loads(request.body)
        # try:
        id = content["binVO"]
        import_href = f"/api/bins/{id}/"
        print(BinVO.objects.all())
        bins = BinVO.objects.get(import_href=import_href)
        content["binVO"] = bins

        # except BinVO.DoesNotExist:
            # return JsonResponse({"message": "invalid id"}, status=400)
        print(content)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET"])
def show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(pk=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    else:
        count, _ = Shoe.objects.get(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def list_all_bins(request):
    if request.method == "GET":
        bins = BinVO.objects.all()
        return JsonResponse(
            bins,
            encoder=BinDetailEncoder,
            safe=False
        )
    else: #POST
        content = json.loads(request.body)
        bins = BinVO.objects.create(**content)
        return JsonResponse(
            bins,
            encoder=BinDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def show_bin(request, pk):
    if request.method == "GET":
        bins = BinVO.objects.get(pk=pk)
        return JsonResponse(
            bins,
            encoder=BinDetailEncoder,
            safe=False
        )
    else:
        count, _ = BinVO.objects.get(pk=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
