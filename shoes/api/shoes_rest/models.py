from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(unique=True, null=False, max_length=200)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"

    class Meta:
        ordering = ("closet_name", "bin_number", "bin_size")


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=50, null=True)
    model_name = models.CharField(max_length=50, null=True)
    color = models.CharField(max_length=20, null=True)
    image = models.URLField(null=True)
    binVO = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )

    def __str__(self):
        return f'{self.manufacturer} {self.model_name} {self.binVO}'
