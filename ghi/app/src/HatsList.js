import React, { Component } from 'react'
import './components/App.css'


class HatsList extends Component {

    constructor(props) {
      super(props)

      this.state = {
         hatsList: []
      }
    }

    async componentDidMount() {
        const hats_list_url = "http://localhost:3000/api/hats/";

        try{
            const response = await fetch(hats_list_url);
            if(response.ok) {
                const hatsListData = await response.json();
                this.setState({
                    hatsList: hatsListData.hats
                })
            }

        } catch (e) {
            console.error(e);
        }
    }

  render() {
    return (
      <div className="app-container">
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Style Name </th>
                    <th>Color</th>
                    <th>Fabric</th>
                    <th>Wardrobe Location</th>
                </tr>
            </thead>
        </table>
        <tbody>
            {this.state.hatsList.map( (hat) => {
            return(
                <tr>
                    <td>{hat.name}</td>
                    <td>{hat.style_name}</td>
                    <td>{hat.color}Color</td>
                    <td>{hat.fabric}</td>
                    <td>{hat.LocationVO.closet_name}</td>
                </tr>)
            })}

        </tbody>
        
      </div>
    )
  }
}

export default HatsList;




// import React from 'react';

// class HatsList extends React.Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             name: '',
//             fabric: '',
//             style_name: '',
//             color: '',
//             hats:[],
//         };

//     async hats_list()    



//     render() {
//         return (
//             <table className="table table-striped">
//                 <thead>
//                     <tr>
//                     <th>Hats List</th>
//                     </tr>
//                 </thead>
//                 <tbody>
                    
//                     {props.attendees.map(attendee => {
//                     return (
//                         <tr key={attendee.href}>
//                         <td>{ attendee.name }</td>
//                         <td>{ attendee.conference }</td>
//                         </tr>
//                     );
//                     })}
//                 </tbody>
//             </table>
//             );
//         };
//     };
// }
