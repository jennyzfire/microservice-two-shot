from django.urls import path
from .views import list_all_shoes, show_shoe

urlpatterns = [
    path("", list_all_shoes, name="list_shoes_url"),
    path("<int:pk>/", show_shoe, name="show_shoe_url"),
    path("bins/<int:bin_vo_id>/shoes/", list_all_shoes, name="hi")
]
