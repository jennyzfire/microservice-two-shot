import React, { Component, useState } from 'react'

class ShoeForm extends Component {

    constructor(props) {
      super(props)

      this.state = {
         manufacturer: "",
         model_name: "",
         color: "",
         binVO: "",
         image: "",
         binVOS: []
      }
    }
;
    async componentDidMount() {
        const url = "http://localhost:8100/api/bins/"
        let response = await fetch(url)
        if(response.ok){
            let data = await response.json();
            this.setState({"binVOS": data.bins}) // entire list of bins
        }
    }

    handleAddField = (event) => {
        let fieldName = event.target.name // i.e. brand, model, etc.
        let fieldValue = event.target.value;

        this.setState({
            [fieldName]: fieldValue
        })
    }

    handleAddSubmit = async (event) => {
        event.preventDefault();
        const incomingData = {...this.state};
        console.log(incomingData)
        delete incomingData.binVOS
        console.log(incomingData);
        const postingShoeUrl = "http://localhost:8080/shoes/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(incomingData),
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(postingShoeUrl, fetchConfig);
        if(response.ok) {
            const newShoe = await response.json();
            console.log("new shoe:", newShoe)
        }

        const cleared = {
            manufacturer: "",
            model_name: "",
            color: "",
            binVO: "",
            image: "",
        };
        this.setState(cleared);
    }


  render() {
    return (
        <div>
            <h2>Add Shoe</h2>
            <form onSubmit={this.handleAddSubmit}>
                <input
                type="text"
                name="manufacturer"
                required="required"
                placeholder="Enter brand name"
                value = {this.state.manufacturer}
                className="form-control"
                id="Brand"
                onChange={this.handleAddField}
                />
                <input
                type="text"
                name="model_name"
                required="required"
                placeholder="Enter shoe model"
                className="form-control"
                value = {this.state.model_name}
                onChange={this.handleAddField}
                id="Model"
                />
                <input
                type="text"
                name="color"
                required="required"
                className="form-control"
                placeholder="Enter Color"
                value = {this.state.color}
                onChange={this.handleAddField}
                id="color"
                />
                <select onChange={this.handleAddField} value={this.state.binVO} name="binVO">
                    {this.state.binVOS.map( (binVO) => {
                        return (
                            <option value={binVO.id} key={binVO.id}>{binVO.id}</option>
                        )
                    })}
                </select>
                <input
                type="text"
                name="image"
                className="form-control"
                placeholder="Enter Image Link"
                id="Image Link"
                value = {this.state.image}
                onChange={this.handleAddField}
                />
                <button type="submit">Add Shoe</button>
            </form>
        </div>
    );
  }
}

export default ShoeForm
