import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root')); 
// root is equal to the entire HTML (reactDom) which is a library imported above // createRoot is a method from the react dom library
//const nav = document.getElementById('nav');

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
