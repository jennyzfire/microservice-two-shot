import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatsForm from './HatsForm';
import HatsList from './HatsList';
import Nav from './Nav';
import ShoesMain from './components/ShoesMain';
//import ShoeClass from './components/ShoeClass';

const App = () => {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatsForm />} />
          <Route path="/hatslist" element={<HatsList />} />
          <Route path="/shoes" element={<ShoesMain />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
