from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Hat, LocationVO
from common.json import ModelEncoder

# Here is my super cool Hat List encoder
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["name", "color", "id"]
    

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "fabric", 
        "style_name", 
        "color", 
        # "picture_url", 
        "location", 
        ]
    encoders = {"location": LocationVOEncoder()}

# Create your views here.
#Need a GET POST AND DELETE

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    """
    Lists the hat names and the link to the hats.

    Returns a dictionary with a single key "hats" which
    is a list of hat names and URLS. Each entry in the list
    is a dictionary that contains the name of the hat and
    the link to the hats information.

    {
        "hats": [
            {
                "name": hats name,
                "href": URL to the hats,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse( {
            "hats": hats },
            encoder=HatListEncoder,
            safe = False,
        )
    else: # This is now our post request
        content = json.loads(request.body)

        # try:
        # we're setting id to the location property entered in the form
        #then we are making a new variable which put the location id into an accetable form using an f string
        #now we are using that href format to check if any of our value object hrefs are the same as what was entered
        #AKA is there a matching location
        #if so we're setting the location property of our newly created hat to all of the location details for that pk
        id = content["location"] 
        print(LocationVO.objects.all())
        location_href = f'/api/locations/{id}/'
        location = LocationVO.objects.get(import_href = location_href) #filtering for a matching import_href
        content["location"] = location
        # except LocationVO.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Check your code again dum dum"},
        #         status=400,
        #     )

        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods (["DELETE", "GET"])
def api_show_hats(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            {"hats": hat },
            encoder=HatDetailEncoder,
        )
    else: 
        print(Hat.objects.all)
        try:
            count, _ = Hat.objects.get(pk=pk).delete()
            return JsonResponse(
                {
                    "deleted": count>0
                }
                )
        except hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
