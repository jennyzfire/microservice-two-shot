import React, { Component } from 'react'
import './App.css'
import ShoeForm from './ShoeForm'

class ShoeClass extends Component {

    constructor(props) {
      super(props)

      this.state = {
         shoesList: []
      }
    }

    async componentDidMount() {
        const listOfShoesUrl = "http://localhost:8080/shoes/";

        try{
            const response = await fetch(listOfShoesUrl);
            if(response.ok) {
                const shoesListData = await response.json();
                this.setState({
                    shoesList: shoesListData.shoes
                })
            }

        } catch (e) {
            console.error(e);
        }
    }

  render() {
    return (
      <div className="app-container">
        <table>
            <thead>
                <tr>
                    <th>Brand (manufacturer)</th>
                    <th>Model (model_name)</th>
                    <th>Color</th>
                    <th>Image</th>
                    <th>Bin Bumber</th>
                </tr>
            </thead>
        </table>
        <tbody>
            {this.state.shoesList.map( (shoe) => {
            return(
                <tr>
                    <td>{shoe.manufacturer}</td>
                    <td>{shoe.model_name}</td>
                    <td>{shoe.color}Color</td>
                    <td>{shoe.image}</td>
                    <td>{shoe.binVO.bin_number}</td>
                </tr>)
            })}

        </tbody>
        <ShoeForm />
      </div>
    )
  }
}

export default ShoeClass
